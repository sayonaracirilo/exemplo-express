const media = require("../media");
const final = require("../final");

module.exports = app => {
  const HomeController = {
    index(req, res) {
      res.render("home/index");
    },
    login(req, res) {
      const { usuario } = req.body;
      const { nota1, nota2, nota3 } = usuario;
      if (nota1 && nota2 && nota3) {
        console.log(req.body);
        req.session.usuario = usuario;
        result = media(nota1, nota2, nota3);
        console.log(result);
        params = final(result);
        res.render("home/resultado", { params, result });
      } else {
        res.redirect("/");
      }
    },
    logout(req, res) {
      req.session.destroy();
      res.redirect("/");
    }
  };
  return HomeController;
};
