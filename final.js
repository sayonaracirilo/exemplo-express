function final(result) {
  if (result >= 5.0) return "Parabéns, você foi aprovado!!";
  else if (result < 3) return "Tente outra vez, você foi reprovado!!";
  else return "Boa sorte, você está na prova final!!";
}
module.exports = final;
