const final = require("../final");
const media = require('../media');

jest.mock('../media'); 

media.mockImplementation(() => 3);
media();

test(" Resultado de media 4",() => {
    expect(final(media())).toEqual("Boa sorte, você está na prova final!!")
})