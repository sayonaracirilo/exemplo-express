const express = require("express");
const path = require("path");
const consign = require("consign");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const expressSession = require("express-session");
const methodOverride = require("method-override");

var app = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");
app.use(
  expressSession({ resave: true, saveUninitialized: true, secret: "tapioca" })
);
app.use(express.urlencoded());
app.use(express.static(__dirname + "/public"));

consign({})
  .include("controllers")
  .then("routes")
  .into(app);

app.listen(3000, () => console.log("Servidor no ar."));
