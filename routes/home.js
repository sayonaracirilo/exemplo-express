module.exports = (app) => {
    const { home } = app.controllers;
    app.get('/', home.index);
    app.post('/resultado', home.login);
    app.get('/voltar', home.logout);
  };